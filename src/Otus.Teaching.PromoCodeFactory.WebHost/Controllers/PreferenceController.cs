﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IRepository<Preference> preferenceRepository;

        public PreferenceController(IMapper mapper, IRepository<Preference> preferenceRepository)
        {
            this.mapper = mapper;
            this.preferenceRepository = preferenceRepository;
        }

        ///<summary>
        ///Получить все существующие предпочтения пользователей
        ///</summary>
        [HttpGet]
        public async Task<ActionResult<PreferenceResponse>> GetPreferencesAsync()
            => Ok(mapper.Map<IEnumerable<PreferenceResponse>>(await preferenceRepository.GetAllAsync()));
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IRepository<Customer> customersRepository;
        private readonly IRepository<PromoCode> promoCodeRepository;
        private readonly IRepository<Preference> preferenceRepository;

        public CustomersController(IMapper mapper, 
                                    IRepository<Customer> customersRepository,
                                    IRepository<PromoCode> promoCodeRepository,
                                    IRepository<Preference> preferenceRepository)
        {
            this.mapper = mapper;
            this.customersRepository = customersRepository;
            this.promoCodeRepository = promoCodeRepository;
            this.preferenceRepository = preferenceRepository;
        }

        ///<summary>
        ///Получить информации о всех пользователях
        ///</summary>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await customersRepository.GetAllAsync();
            var customersShortresponce = mapper.Map<IEnumerable<CustomerShortResponse>>(customers);
            return Ok(customersShortresponce);
        }
        
        ///<summary>
        ///Получить информацию о пользователе по Id
        ///</summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await customersRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();
            var customerResponce = mapper.Map<CustomerResponse>(customer);
            return Ok(customerResponce);
        }
        
        ///<summary>
        ///Создать нового пользователя
        ///</summary>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = mapper.Map<Customer>(request);
            if(request.PreferenceIds != null)
            {
                var preferences = await preferenceRepository.GetAllAsync(c => request.PreferenceIds.Contains(c.Id));
                customer.Preferences = preferences.ToList();
            }

            await customersRepository.CreateAsync(customer);
            await customersRepository.SaveChangesAsync();
            return Ok(customer.Id);
        }
        
        ///<summary>
        ///Редактировать пользователя
        ///</summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await customersRepository.GetByIdAsync(id);
            if(customer == null)
                return NotFound();

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            if(request.PreferenceIds != null)
            {
                //Removing preferences that are not in the request
                customer.Preferences = customer.Preferences.Where(cp => request.PreferenceIds.Contains(cp.Id)).ToList();

                //Adding new preferences
                var newPreferencesIds = request.PreferenceIds.Where(pid => !customer.Preferences.Any(cp => cp.Id == pid));
                var newPreferences = await preferenceRepository.GetAllAsync(c => newPreferencesIds.Contains(c.Id));
                customer.Preferences = customer.Preferences.Concat(newPreferences).ToList();
            }
            else
                customer.Preferences.Clear();

            await customersRepository.UpdateAsync(customer);
            await customersRepository.SaveChangesAsync();
            return Ok();
        }
        
        ///<summary>
        ///Удалить пользователя
        ///</summary>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await customersRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();
            foreach (var promoCode in customer.PromoCodes)
            {
                await promoCodeRepository.DeleteAsync(promoCode);
            }
            await customersRepository.DeleteAsync(customer);
            await customersRepository.SaveChangesAsync();

            return Ok();

        }
    }
}
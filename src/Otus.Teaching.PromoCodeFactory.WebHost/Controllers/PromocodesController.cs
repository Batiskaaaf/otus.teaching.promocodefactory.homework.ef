﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IRepository<PromoCode> promocodeRepository;
        private readonly IRepository<Customer> customerRepository;
        private readonly IRepository<Preference> preferenceRepository;
        private readonly IRepository<Employee> employeeRepository;

        public PromocodesController(IMapper mapper,
            IRepository<PromoCode> promocodeRepository,
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository,
            IRepository<Employee> employeeRepository)
        {
            this.mapper = mapper;
            this.promocodeRepository = promocodeRepository;
            this.customerRepository = customerRepository;
            this.preferenceRepository = preferenceRepository;
            this.employeeRepository = employeeRepository;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
            => Ok(mapper.Map<IEnumerable<PromoCodeShortResponse>>(await promocodeRepository.GetAllAsync()));

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preference = await preferenceRepository.GetFristOrDefaultAsync(p => p.Name == request.Preference);
            if(preference == null)
            {
                preference = new Preference { Name = request.Preference };
                await preferenceRepository.CreateAsync(preference);
            }

            var customersWithPreference = await customerRepository.GetAllAsync(c => c.Preferences.Any(p => p.Name == request.Preference));
            foreach (var customer in customersWithPreference)
            {
            var promoCode = new PromoCode()
            {
                Code = request.PromoCode,
                ServiceDescription = request.ServiceDescription,
                BeginDate = DateTime.UtcNow,
                EndDate = DateTime.UtcNow.AddMonths(1),
                PartnerName = request.PartnerName,
                Preference = preference
            };
                customer.PromoCodes.Add(promoCode);
            }
            await customerRepository.SaveChangesAsync();
            return Ok();

        }
    }
}
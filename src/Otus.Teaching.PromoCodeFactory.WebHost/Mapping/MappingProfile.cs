using System;
using System.Linq;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapping;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<DateTime, string>().ConvertUsing<DateTimeConverter>();
        CreateMap<Customer, CustomerShortResponse>();
        CreateMap<Customer, CustomerResponse>();
        CreateMap<CreateOrEditCustomerRequest, Customer>();
        CreateMap<Preference, PreferenceResponse>();
        CreateMap<PromoCode, PromoCodeShortResponse>();
        CreateMap<GivePromoCodeRequest, PromoCode>().ForMember(pc => pc.Code, opt => opt.MapFrom(src => src.PromoCode));
    }

    public class DateTimeConverter : ITypeConverter<DateTime, string>
    {
        public string Convert(DateTime source, string destination, ResolutionContext context)
        {
            return source.ToString("yyyy-MM-ddTHH:mm:ss");
        }
    }
}
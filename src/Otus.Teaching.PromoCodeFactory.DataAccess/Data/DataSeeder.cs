
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data;

public static class DataSeeder
{
    public static void SeedDb(AppDbContext context)
    {
        context.Database.EnsureDeleted();
        context.Database.EnsureCreated();

        context.Customers.AddRange(FakeDataFactory.Customers);
        context.SaveChanges();

        context.Employees.AddRange(FakeDataFactory.Employees);
        context.SaveChanges();
    }
}